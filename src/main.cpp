#include <iostream>
#include "restclient-cpp/restclient.h"
#include <nlohmann/json.hpp>
#include <string>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/core/types.hpp>


// Strongly based on JC's solution for fetching images
std::string fetch_images()
{
    // Fetch the url from aws.random
    RestClient::Response response = RestClient::get("https://aws.random.cat/meow");
    std::cout << response.body << std::endl;

    // Convert to json to make lives easier
    nlohmann::json json_body = nlohmann::json::parse(response.body);
    std::cout << json_body["file"] << std::endl;

    // Download file
    std::string folder_path = "../original";
    std::string img_name = json_body["file"].dump();
    std::string wget_command = "wget -P " + folder_path + " " + img_name;
    system(wget_command.c_str()); // cat get!!

    // Openfile and drop "/" from image name
    std::string file_name = img_name.substr(img_name.find("i/")+2);
    file_name = file_name.substr(0, file_name.size()-1);// Chop off lingering '"'
    std::cout << file_name << std::endl;
    return file_name;
}

cv::Mat reduce_image_size(std::string filename)
{
    // Location for original images that we want to process
    std::string folder_path = "../original/";
    std::string scaled_path = "../scaled/";
    std::string original_image = folder_path + filename;
    std::string scaled_image = scaled_path + filename;
    // Create Mat object
    cv::Mat img = cv::imread(original_image); 
    cv::Mat scaled_img;
    float img_ratio = (float) img.size().width / (float) img.size().height;
    int scale_h = 300;
    int scale_w = 300;

    std::cout << "Original width: " << img.size().width << std::endl;
    std::cout << "Original height: " << img.size().height << std::endl;
    std::cout << "Image ratio: " << img_ratio << std::endl;

    if (img.size().width > img.size().height) {
        scale_h = (int) (scale_h / img_ratio);
    } else if (img.size().height > img.size().width) {
        scale_w = (int) (scale_w * img_ratio);
    }

    std::cout << "Scaled width: " << scale_w << std::endl;
    std::cout << "Scaled height: " << scale_h << std::endl;

    cv::resize(img, scaled_img, cv::Size(scale_w, scale_h), 0, 0, cv::INTER_LINEAR);

    if (img.empty())
    {
        std::cout << "Problems with file: " << original_image << std::endl;
    } 

    return scaled_img;
}

cv::Mat add_text(cv::Mat& src_img)
{
    cv::Point org;
    org.x = 5;
    org.y = 15;

    cv::putText(src_img, "Elmo Rohula", org, 5, 1, cv::Scalar(255,255,0), 2, 8, false);
    org.x = 5;
    org.y = 40;

    cv::putText(src_img, "Rainer Waltzer", org, 5, 1, cv::Scalar(255,255,0), 2, 8, false);

    return src_img;
}

cv::Mat bw_transform(cv::Mat& src_img) {
    std::cout << "Crush the image" << std::endl;

    cv::Mat img = src_img;
    cv::Mat crushed;

    cv::cvtColor(img, crushed, cv::COLOR_BGR2GRAY);
    cv::threshold(crushed, crushed, 127, 255, 0);
    //Back to color so that text can be color text
    cv::cvtColor(crushed, crushed, cv::COLOR_GRAY2BGR);

    if (!img.empty()) {
        std::cout << "Image crushed succesfully" << std::endl;
    } else {
        std::cout << "An error has occured" << std::endl;
    }
    return crushed;
}

int main()
{
    std::string original_file_name;
    std::cout << "Let's go and process some images!" << std::endl;
    original_file_name = fetch_images();

    std::cout << "Original: " << original_file_name << std::endl;
    cv::Mat modified_image;
    modified_image = reduce_image_size(original_file_name);
    if (modified_image.empty())
    {
        std::cout << "Problems...";
        return -1;
    }
    
    modified_image = bw_transform(modified_image);
    modified_image = add_text(modified_image);
    
    cv::imshow("Display window", modified_image);
    //  This is required to show the picture
    int k = cv::waitKey(0);
    // static typed badness :)
    std::string output_filename = "../output/crushed-";
    output_filename.append(original_file_name);

    std::cout << "Filename to write: " << output_filename << std::endl;
    cv::imwrite(output_filename, modified_image);
    return 0;

}
