# Processing Images

This is a solution for an assignment in the CPP Embedded Development Bootcamp. The program retrieves a random cat picture from the internet and does all sorts of image manipulations on it.

## Table of Contents

 - [Requirements](#requirements)
 - [Usage](#usage)
 - [Maintainers](#maintainers)
 - [Contributing](#contributing)
 - [License](#license)

## Requirements

Requires `gcc`, `reastclient-cpp`, `nlohmann::json`, `opencv`

## Usage

Good luck building!

    $ cd build
    $ ./main

## Maintainers

[**Elmo Rohula @rootElmo**](https://gitlab.com/rootElmo)
[**Rainer Waltzer @raiwal**](https://gitlab.com/raiwal)

## Contributing

## License
